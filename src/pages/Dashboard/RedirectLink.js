import React,{useState} from 'react';
import { connect } from "react-redux";

class RedirectLink extends React.Component {  
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoading: true,
      data:[]
    };
  }
  
  componentDidMount() {
    this.setState({isLoading:true});

    const { match: { params } } = this.props;

      fetch(window.location.hostname+'/RedirectLink/?'+window.location.search.substr(2))
      .then(res => res.json())
      .then(
      (result) => {
        this.findGetParameter(window.location.search.substr(2))
      },
        (error) => {
          this.findGetParameter(window.location.search.substr(2))
        }
      )

}

 findGetParameter(parameterName) {
  var result = null,
      tmp = [];
  location.search
      .substr(1)
      .split("&")
      .forEach(function (item) {
        tmp = item.split("=");
        
        if (tmp[0] === 'shortlink') {
          result = decodeURIComponent(window.location.search.substr(11)); 
        }
      });
    //   console.log(result)
      return window.location.href = 'http://'+result;
//    return window.location.replace(result);

}
  
	render() {
    
		return(
      <div className=''>
          
      </div>
		);
	}
  
}

function mapStateToProps(state) {
  return { loginUser: state.authReducer.loginUser }
}

function mapDispatchToProps(dispatch) {
  return { dispatch }
}

export default connect(mapStateToProps, mapDispatchToProps)(RedirectLink);
