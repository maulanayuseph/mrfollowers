import React,{useState} from 'react';
import "../../App.scss";
import "../../AppMobile.scss";
import { connect } from "react-redux";
import { BallBeat } from 'react-pure-loaders';
import pengusaha from "../../assets/img/home/html/pengusahailustrasi.svg"
import pegawai from "../../assets/img/home/html/pekerjailustrasi.svg"
import berita1 from "../../assets/img/home/thought-catalog-UK78i6vK3sc-unsplash@2x.png"
import berita2 from "../../assets/img/home/glenn-carstens-peters-SL5d_8ywAAA-unsplash@2x.png"
import berita3 from "../../assets/img/home/dayne-topkin-y5_mFlLMwJk-unsplash@2x.png"
import wa from '../../assets/img/wa.png'
import ig from '../../assets/img/ig.jpg'
import gm from '../../assets/img/gm.png'
import fb from '../../assets/img/fb.png'
import { BASE_URL } from "../../service/api";
import bni from '../../assets/img/bni.png'
import mandiri from '../../assets/img/mandiri.jpg'
import ovo from '../../assets/img/ovo.png'
import gopay from '../../assets/img/gopay.jpg'
import dana from '../../assets/img/dana.png'
import Background1 from '../../assets/img/headerig.jpg'
import ig_paket from '../../assets/img/instagram-1882330_1280.png'
import Navbar from "../../components/navbar_home";
import Footer from "../../components/Footer";
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Carousel from 'react-bootstrap/Carousel'
import slideimg from '../../assets/img/home/smartphone_mockup@2x.png';
import terlaris from '../../assets/img/home/html/onstore/Group 15276.svg';
import $ from 'jquery'
import axios from 'axios'

import { Divider, Image, Icon, Grid, Menu, Segment, Header, Card , Button, Form, Dropdown, Label, Message} from 'semantic-ui-react'

class Orderfollowersig extends React.Component {  
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoading: true,
      data:[],
      nama : '',
      email : '',
      no_tlp : '',
      id_layanan : '',
      username : '',
      id_pembayaran : '',
      msgError : '',
      msgSuccess : ''
    };
  }
  
  componentDidMount() {
    this.setState({isLoading:true});
}
  
	render() {
        const bayarOptions = [
            {
                key : '1',
                text : 'BNI',
                value : '1',
                image: { src: bni, size:'mini' },
            },
            {
                key : '2',
                text : 'Mandiri',
                value : '2',
                image: { src: mandiri, size:'mini' },
            },
            {
                key : '3',
                text : 'OVO',
                value : '3',
                image: { src: ovo, size:'mini' },
            },
            {
                key : '4',
                text : 'Gopay',
                value : '4',
                image: { src: gopay, size:'small' },
            },
            {
                key : '5',
                text : 'Dana',
                value : '5',
                image: { src: dana, size:'small' },
            }
        ]

        const paketOptions = [
            {
              key: '1',
              text: '300 Followers - Rp. 30.000',
              value: '1',
              image: { avatar: true, src: ig },
            },
            {
                key: '2',
                text: '500 Followers - Rp. 50.000',
                value: '2',
                image: { avatar: true, src: ig },
            },
            {
                key: '3',
                text: '1000 Followers - Rp. 100.000',
                value: '3',
                image: { avatar: true, src: ig },
            },
            {
                key: '4',
                text: '2500 Followers - Rp. 200.000',
                value: '4',
                image: { avatar: true, src: ig },
            },
            {
                key: '5',
                text: '5000 Followers - Rp. 380.000',
                value: '5',
                image: { avatar: true, src: ig },
            },
            {
                key: '6',
                text: '10000 Followers - Rp. 750.000',
                value: '6',
                image: { avatar: true, src: ig },
            },
            {
                key: '7',
                text: '300 Followers (Garansi) - Rp. 50.000',
                value: '7',
                image: { avatar: true, src: ig },
            },
            {
                key: '8',
                text: '500 Followers (Garansi) - Rp. 100.000',
                value: '8',
                image: { avatar: true, src: ig },
            },
            {
                key: '9',
                text: '1000 Followers (Garansi) - Rp. 200.000',
                value: '9',
                image: { avatar: true, src: ig },
            },
            {
                key: '10',
                text: '2500 Followers (Garansi) - Rp. 380.000',
                value: '10',
                image: { avatar: true, src: ig },
            },
            {
                key: '11',
                text: '5000 Followers (Garansi) - Rp. 750.000',
                value: '11',
                image: { avatar: true, src: ig },
            },
            {
                key: '12',
                text: '10000 Followers (Garansi) - Rp. 1.800.000',
                value: '12',
                image: { avatar: true, src: ig },
            },
          ]
        
        const handleTextChange = event => this.setState({
            [event.target.name]: event.target.value
        });

        const getBird = (event, {value}) => {
            this.setState({
                id_pembayaran: value
            });
        }

        const getLayanan = (event, {value}) => {
            this.setState({
                id_layanan: value
            });
        }

        const form = {
            nama : this.state.nama,
            email : this.state.email,
            no_tlp : this.state.no_tlp,
            id_layanan : this.state.id_layanan,
            username : this.state.username,
            id_pembayaran : this.state.id_pembayaran
        }

        async function handleInputSubmit () {
            var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

            if (form.nama === "") {
                $("#errnama").removeAttr('style');
                window.setTimeout(function(){
                    $("#errnama").attr('style','display:none')
                },2000)
                return false;
            }

            if(form.email.match(mailformat)) {
                
            } else{
                $("#erremail").removeAttr('style');
                window.setTimeout(function(){
                    $("#erremail").attr('style','display:none')
                },2000)
                return false;
            }

            if(form.no_tlp === "") {
                $("#errno_tlp").removeAttr('style');
                window.setTimeout(function(){
                    $("#errno_tlp").attr('style','display:none')
                },2000)
                return false;
            }

            if(form.username === "") {
                $("#errusername").removeAttr('style');
                window.setTimeout(function(){
                    $("#errusername").attr('style','display:none')
                },2000)
                return false;
            }

            if(form.id_layanan === "") {
                $("#errpaketlayanan").removeAttr('style');
                window.setTimeout(function(){
                    $("#errpaketlayanan").attr('style','display:none')
                },2000)
                return false;
            }

            if(form.id_pembayaran === "") {
                $("#errpembayaran").removeAttr('style');
                window.setTimeout(function(){
                    $("#errpembayaran").attr('style','display:none')
                },2000)
                return false;
            }

            submitOrder(form)
        }

        const submitOrder = async (values) => {
            $("#btnOrder").addClass('loading');
            $("#btnOrder").addClass('disabled');

            let config = {
                headers: {
                //   'Authorization' : 'Basic d2ViMTp3ZWIx',
                  'content-type': 'application/json',
                  'Access-Control-Allow-Origin': '*',
                  "Access-Control-Allow-Headers" : "Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With",
                  "Access-Control-Allow-Methods" : "POST"
                },
                auth : {
                    username : 'web1',
                    password : 'web1'
                }
            }

            axios.post(BASE_URL+"/Order", values, config)
            .then((resp) => {
              
                if (resp.data.status != 'success') {
                    $("#btnOrder").removeClass('loading');
                    $("#btnOrder").removeClass('disabled');
                    $("#classError").removeAttr('style');
                    this.setState({ 
                        msgError : resp.data.message
                    })
                    
                    setTimeout(() => {
                        $("#classError").attr('style','display:none');
                    }, 2000)
                } else {
                    $("#btnOrder").removeClass('loading');
                    $("#btnOrder").removeClass('disabled');
                    $("#classSuccess").removeAttr('style');
                    this.setState({ 
                        msgSuccess : resp.data.message
                    })

                    setTimeout(() => {
                       window.location.reload();
                    }, 10000)
                }
            })
            .catch((resp) =>{
                $("#classError").removeAttr('style');
                $("#btnOrder").removeClass('loading');
                $("#btnOrder").removeClass('disabled');
                
                this.setState({ 
                    msgError : 'Terjadi Kesalahan '
                })
                
                setTimeout(() => {
                    $("#classError").attr('style','display:none');
                }, 2000)

            });
            
        }

		return(
      <div className=''>
          <Navbar/>
          <HeaderDiv/>
          <Paket paketOptions={paketOptions} bayarOptions={bayarOptions} handleInputSubmit={handleInputSubmit} form={form} 
          handleTextChange={handleTextChange}  getBird={getBird} getLayanan={getLayanan} msgSuccess={this.state.msgSuccess} msgError={this.state.msgError}/>
          <Pesanviawa/>
          <br/><br/>
          <Footer/>
      </div>
		);
	}
  
}

function HeaderDiv() {
  return (
    <section id="intro">

        <div className="intro-content">
          <h2>Instagram</h2>
          <h1 className="headtextOrder">
            <i>
                Kembangkan akun Instagram anda dengan beberapa layanan <br/> yang kami punya !
          </i>
          </h1>
        </div>

        <div id="intro-carousel" className="owl-carousel" >
          <div className="item" style={{backgroundImage:"url(" + Background1 + ")"}}></div>
        </div>

      </section>
  );
}

function Paket({paketOptions,bayarOptions,handleInputSubmit,form,handleTextChange,getBird,getLayanan,msgSuccess,msgError}) {
    return (
        <Container style={{paddingTop:'20px'}}>
        <Row>
        <Col xs="12" md="12" lg="12">
            <Header as="h1">
                Form Order Paket Tambah Followers Instagram
            </Header>
            <Form>
                <Form.Field>
                <label>Nama *</label>
                <input placeholder='Nama' name="nama" value={form.nama} onChange={handleTextChange} />
                <Label basic color='red' pointing id="errnama" style={{display:'none'}}>
                    Nama Wajib Diisi
                </Label>
                </Form.Field>
                <Form.Field>
                <label>Email *</label>
                <input placeholder='Email' name="email" value={form.email} onChange={handleTextChange}/>
                <Label basic color='red' pointing id="erremail" style={{display:'none'}}>
                    Harap masukkan email dengan benar
                </Label>
                </Form.Field>
                <Form.Field>
                <label>Nomor Telepon *</label>
                <input placeholder='Nomor Telepon' type="number" name="no_tlp" value={form.no_tlp} onChange={handleTextChange}/>
                <Label basic color='red' pointing id="errno_tlp" style={{display:'none'}}>
                    Nomor Telepon Wajib Diisi
                </Label>
                </Form.Field>
                <Form.Field>
                <label>Username Instagram *</label>
                <input placeholder='Username Instagram' name="username" value={form.username} onChange={handleTextChange}/>
                <span><b>Harap akun tidak di private selama proses</b></span>
                <Label basic color='red' pointing id="errusername" style={{display:'none'}}>
                    Username Wajib Diisi
                </Label>
                </Form.Field>
                <Form.Field>
                <label>Paket Layanan *</label>
                <Dropdown
                placeholder='Pilih Layanan Paket'
                fluid
                name="id_layanan"
                onChange={getLayanan}
                selection
                options={paketOptions}
                />
                <Label basic color='red' pointing id="errpaketlayanan" style={{display:'none'}}>
                    Paket Layanan Wajib Diisi
                </Label>
                </Form.Field>
                <Form.Field>
                    <label>Pembayaran VIA *</label>
                    <Dropdown
                    placeholder='Pembayaran VIA'
                    fluid
                    name="id_pembayaran"
                    onChange={getBird}
                    selection
                    options={bayarOptions}
                    />
                <Label basic color='red' pointing id="errpembayaran" style={{display:'none'}}>
                    Pembayaran VIA Wajib Diisi
                </Label>
                </Form.Field>
                <Button onClick={handleInputSubmit} color="green" circular id="btnOrder">Order</Button>
            </Form>
            <Message positive id="classSuccess" style={{display:'none'}}>
                <Message.Header>{msgSuccess}</Message.Header>
                <p>
                Terima kasih telah melakukan pembelian, Silahkan cek email kotak masuk anda atau cek spam kotak masuk anda terlebih dahulu sebelum melakukan pembayaran..
                </p>
            </Message>
            <Message color="red" id="classError" style={{display:'none'}}>
                <Message.Header>{msgError}</Message.Header>
            </Message>
            <br/><br/>
        </Col>
        </Row>
        </Container>
    )
}

function Pesanviawa() {
    return (
        <Container style={{paddingTop:'20px'}}>
        <Row>
        <Col xs="12" md="12" lg="12">
        <section id="call-to-action" className="wow fadeInUp">
        <div className="container">
            <div className="row">
            <div className="col-lg-9 text-center text-lg-left">
                <h3 className="cta-title">Pesan via WhatsApp</h3>
                <p className="cta-text"> 
                    Silahkan Klik tombol jika anda ingin memesan via WhatsApp, Silahkan infokan nama paket layanan yang akan dibeli.
                </p>
            </div>
            <div className="col-lg-3 cta-btn-container text-center">
                <a className="cta-btn align-middle" href="https://api.whatsapp.com/send?phone=6285846104311&text=Hai Mr followers, saya ingin pesan layanan">Pesan Sekarang</a>
            </div>
            </div>

        </div>
        </section>
        <br/><br/>
        </Col>
        </Row>
        </Container>
    )
}


function mapStateToProps(state) {
  return { loginUser: state.authReducer.loginUser }
}

function mapDispatchToProps(dispatch) {
  return { dispatch }
}

export default connect(mapStateToProps, mapDispatchToProps)(Orderfollowersig);
