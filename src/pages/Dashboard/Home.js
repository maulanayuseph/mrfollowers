import React,{useState} from 'react';
import "../../App.scss";
import "../../AppMobile.scss";
import { connect } from "react-redux";
import { BallBeat } from 'react-pure-loaders';
import pengusaha from "../../assets/img/home/html/pengusahailustrasi.svg"
import pegawai from "../../assets/img/home/html/pekerjailustrasi.svg"
import berita1 from "../../assets/img/home/thought-catalog-UK78i6vK3sc-unsplash@2x.png"
import berita2 from "../../assets/img/home/glenn-carstens-peters-SL5d_8ywAAA-unsplash@2x.png"
import berita3 from "../../assets/img/home/dayne-topkin-y5_mFlLMwJk-unsplash@2x.png"
import Background1 from '../../assets/img/slide1.jpg'
import Background2 from '../../assets/img/slide2.jpg'
import Background3 from '../../assets/img/slide3.jpg'
import Background4 from '../../assets/img/slide4.jpg'
import Background5 from '../../assets/img/slide5.jpg'
import aboutus from '../../assets/img/aboutus.jpg'
import Navbar from "../../components/navbar_home";
import { BASE_URL } from "../../service/api";
import Footer from "../../components/Footer";
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Carousel from 'react-bootstrap/Carousel'
import slideimg from '../../assets/img/home/smartphone_mockup@2x.png';
import terlaris from '../../assets/img/home/html/onstore/Group 15276.svg';
import $ from 'jquery'

import { Divider, Image, Icon, Grid, Menu, Segment } from 'semantic-ui-react'

class Home extends React.Component {  
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoading: true,
      data:[]
    };
  }
  
  componentDidMount() {
    this.setState({isLoading:true});

  }
  
	render() {
    
		return(
      <div className=''>
          <Navbar/>
          <Header/>
          <Pendaftaran/>
          <SliderApp />
          {/* <Testimonial/> */}
          <Footer/>
      </div>
		);
	}
  
}

function Header() {
  return (
    <section id="intro">

        <div className="intro-content">
          <h2>Buat <span>Impian anda</span><br/>Menjadi Kenyataan!</h2>
          <h1 className="headerPage">Kami siap membantu anda dalam kebutuhan social media anda</h1>
          {/* <div>
            <a href="#about" className="btn-get-started scrollto">Ya, Saya Mau</a>
            <a href="#services" className="btn-projects scrollto">Jasa Kami</a>
          </div> */}
        </div>

        <div id="intro-carousel" className="owl-carousel" >
          <div className="item" style={{backgroundImage:"url(" + Background1 + ")"}}></div>
          <div className="item" style={{backgroundImage:"url(" + Background2 + ")"}}></div>
          <div className="item" style={{backgroundImage:"url(" + Background3 + ")"}}></div>
          <div className="item" style={{backgroundImage:"url(" + Background4 + ")"}}></div>
          <div className="item" style={{backgroundImage:"url(" + Background5 + ")"}}></div>
        </div>

      </section>
  );
}

function Pendaftaran() {
  return (
    <section id="about" className="wow fadeInUp">
    <div className="container">
      <div className="row">
        <div className="col-lg-6 about-img">
          <img src={aboutus} alt="" />
        </div>

        <div className="col-lg-6 content">
          <h2>Tentang Kami</h2>
          <h3>Kami membantu anda dalam kebutuhan social media anda, ataupun marketplace yang anda miliki.
              Baik di Instagram, Facebook, Twitter, Youtube serta Marketplace Shopee, Tokopedia dan masih banyak lagi !!.
          </h3>

          <ul>
            <li><i className="ion-android-checkmark-circle"></i> Miliki Followers aktif maupun pasif baik di Indonesia ataupun Luar Negeri.</li>
            <li><i className="ion-android-checkmark-circle"></i> Faktanya Social media sekarang sudah menjadi tempat bisnis yang menguntungkan</li>
            <li><i className="ion-android-checkmark-circle"></i> Tingkatkan akun social media anda dan Toko anda dengan layanan yang kami miliki.</li>
            <li><i className="ion-android-checkmark-circle"></i> Pasarkan produk anda melalui dunia digital dan bersiap-siap mendapatkan profit yang anda inginkan.</li>
          </ul>

        </div>
      </div>

    </div>
  </section>
  );
}

function SliderApp() {
  return (
    <section id="services">
      <div className="container">
        <div className="section-header">
          <h2>Jasa</h2>
          <h3>
            Kami memiliki beberapa jasa yang bisa anda butuhkan untuk meningkatkan akun social media anda , marketplace anda, dan raih profit yang anda inginkan.
          </h3>
        </div>

        <div className="row">

          <div className="col-lg-6">
            <div className="box wow fadeInLeft">
              <div className="icon"><i className="fa fa-instagram"></i></div>
              <h4 className="title"><a href="/tambah-followers-instagram">Instagram</a></h4>
              <p className="description">
                Kami memiliki jasa untuk menambah followers instagram, baik local maupun internasional, serta untuk like instagram, views video instagram, views video IGTV
              </p>
            </div>
          </div>

          <div className="col-lg-6">
            <div className="box wow fadeInRight">
              <div className="icon"><i className="fa fa-facebook"></i></div>
              <h4 className="title"><a href="/page-likes-facebook">Facebook</a></h4>
              <p className="description">
                Kami memiliki jasa untuk menambah followers facebook, tambah like facebook, tambah followers fanspage untuk kebutuhan social media facebook anda
              </p>
            </div>
          </div>

          <div className="col-lg-6">
            <div className="box wow fadeInLeft" data-wow-delay="0.2s">
              <div className="icon"><i className="fa fa-instagram"></i></div>
              <h4 className="title"><a href="/auto-likes-instagram">Instagram Auto Likes</a></h4>
              <p className="description">
                Kami memiliki jasa untuk menambah auto likes postingan instagram, sampai dengan 10 postingan baru kalian. 
              </p>
            </div>
          </div>

          <div className="col-lg-6">
            <div className="box wow fadeInRight" data-wow-delay="0.2s">
              <div className="icon"><i className="fa fa-youtube"></i></div>
              <h4 className="title"><a href="subscriber-youtube">Youtube</a></h4>
              <p className="description">
                Kami memiliki jasa untuk menambah subscriber youtube, tambah likes youtube, tambah views youtube, tambah jam tayang dan tambah comment youtube
              </p>
            </div>
          </div>

        </div>

      </div>
    </section>
  );
}

function Berita() {
  return (
    <section id="clients" className="wow fadeInUp">
      <div className="container">
        <div className="section-header">
          <h2>Clients</h2>
          <p>Sed tamen tempor magna labore dolore dolor sint tempor duis magna elit veniam aliqua esse amet veniam enim export quid quid veniam aliqua eram noster malis nulla duis fugiat culpa esse aute nulla ipsum velit export irure minim illum fore</p>
        </div>

        <div className="owl-carousel clients-carousel">
          <img src="img/clients/client-1.png" alt="" />
          <img src="img/clients/client-2.png" alt="" />
          <img src="img/clients/client-3.png" alt=""/>
          <img src="img/clients/client-4.png" alt=""/>
          <img src="img/clients/client-5.png" alt=""/>
          <img src="img/clients/client-6.png" alt=""/>
          <img src="img/clients/client-7.png" alt=""/>
          <img src="img/clients/client-8.png" alt=""/>
        </div>

      </div>
    </section>
  );
}

function Portofolio() {
  return (
    <section id="portfolio" className="wow fadeInUp">
      <div className="container">
        <div className="section-header">
          <h2>Our Portfolio</h2>
          <p>Sed tamen tempor magna labore dolore dolor sint tempor duis magna elit veniam aliqua esse amet veniam enim export quid quid veniam aliqua eram noster malis nulla duis fugiat culpa esse aute nulla ipsum velit export irure minim illum fore</p>
        </div>
      </div>

      <div className="container-fluid">
        <div className="row no-gutters">

          <div className="col-lg-3 col-md-4">
            <div className="portfolio-item wow fadeInUp">
              <a href="img/portfolio/1.jpg" className="portfolio-popup">
                <img src="img/portfolio/1.jpg" alt=""/>
                <div className="portfolio-overlay">
                  <div className="portfolio-info"><h2 className="wow fadeInUp">Portfolio Item 1</h2></div>
                </div>
              </a>
            </div>
          </div>

          <div className="col-lg-3 col-md-4">
            <div className="portfolio-item wow fadeInUp">
              <a href="img/portfolio/2.jpg" className="portfolio-popup">
                <img src="img/portfolio/2.jpg" alt=""/>
                <div className="portfolio-overlay">
                  <div className="portfolio-info"><h2 className="wow fadeInUp">Portfolio Item 2</h2></div>
                </div>
              </a>
            </div>
          </div>

          <div className="col-lg-3 col-md-4">
            <div className="portfolio-item wow fadeInUp">
              <a href="img/portfolio/3.jpg" className="portfolio-popup">
                <img src="img/portfolio/3.jpg" alt=""/>
                <div className="portfolio-overlay">
                  <div className="portfolio-info"><h2 className="wow fadeInUp">Portfolio Item 3</h2></div>
                </div>
              </a>
            </div>
          </div>

          <div className="col-lg-3 col-md-4">
            <div className="portfolio-item wow fadeInUp">
              <a href="img/portfolio/4.jpg" className="portfolio-popup">
                <img src="img/portfolio/4.jpg" alt=""/>
                <div className="portfolio-overlay">
                  <div className="portfolio-info"><h2 className="wow fadeInUp">Portfolio Item 4</h2></div>
                </div>
              </a>
            </div>
          </div>

          <div className="col-lg-3 col-md-4">
            <div className="portfolio-item wow fadeInUp">
              <a href="img/portfolio/5.jpg" className="portfolio-popup">
                <img src="img/portfolio/5.jpg" alt=""/>
                <div className="portfolio-overlay">
                  <div className="portfolio-info"><h2 className="wow fadeInUp">Portfolio Item 5</h2></div>
                </div>
              </a>
            </div>
          </div>

          <div className="col-lg-3 col-md-4">
            <div className="portfolio-item wow fadeInUp">
              <a href="img/portfolio/6.jpg" className="portfolio-popup">
                <img src="img/portfolio/6.jpg" alt=""/>
                <div className="portfolio-overlay">
                  <div className="portfolio-info"><h2 className="wow fadeInUp">Portfolio Item 6</h2></div>
                </div>
              </a>
            </div>
          </div>

          <div className="col-lg-3 col-md-4">
            <div className="portfolio-item wow fadeInUp">
              <a href="img/portfolio/7.jpg" className="portfolio-popup">
                <img src="img/portfolio/7.jpg" alt=""/>
                <div className="portfolio-overlay">
                  <div className="portfolio-info"><h2 className="wow fadeInUp">Portfolio Item 7</h2></div>
                </div>
              </a>
            </div>
          </div>

          <div className="col-lg-3 col-md-4">
            <div className="portfolio-item wow fadeInUp">
              <a href="img/portfolio/8.jpg" className="portfolio-popup">
                <img src="img/portfolio/8.jpg" alt=""/>
                <div className="portfolio-overlay">
                  <div className="portfolio-info"><h2 className="wow fadeInUp">Portfolio Item 8</h2></div>
                </div>
              </a>
            </div>
          </div>

        </div>

      </div>
    </section>
  );
}

function Testimonial() {
  return (
    <section id="testimonials" className="wow fadeInUp">
      <div className="container">
        <div className="section-header">
          <h2>Testimonials</h2>
          <h3>
            Apa kata mereka tentang jasa yang diberikan oleh kami !!
          </h3>
        </div>
        <div className="owl-carousel testimonials-carousel">

            <div className="testimonial-item">
              <h3>Handayani</h3>
              <h4>Jasa Tambah Followers Tokopedia </h4><br/>
              <p>
                Terima kasih telah membantu toko marketplace kami, semoga dapat membawa keberuntungan untuk toko kami.
              </p>
            </div>

            <div className="testimonial-item">
              <h3>Liani Febria</h3>
              <h4>Jasa Tambah Followers Instagram</h4><br/>
              <p>
                Berkat tambah followers instagram, saya makin percaya diri untuk membuat sebuah konten yang baik. 
              </p>
            </div>

            <div className="testimonial-item">
              <h3>Dian Hadi Pratama</h3>
              <h4>Jasa Tambah Followers Instagram</h4><br/>
              <p>
                Terima kasih telah menambah Followers instagram saya.
              </p>
            </div>

            <div className="testimonial-item">
              <h3>Wibowo Putra</h3>
              <h4>Jasa Tambah Subscriber Youtube</h4><br/>
              <p>
                Terima kasih telah menambah subscriber youtube saya, jadi saya semangat bikin konten.
              </p>
            </div>

            <div className="testimonial-item">
              <h3>Bunga Adilla Permata</h3>
              <h4>Jasa Tambah Followers Shopee</h4>
              <br/>
              <p>
                Terima kasih telah menambah followers marketplace saya
              </p>
            </div>

        </div>

      </div>
    </section>
  )
}
function mapStateToProps(state) {
  return { loginUser: state.authReducer.loginUser }
}

function mapDispatchToProps(dispatch) {
  return { dispatch }
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);
