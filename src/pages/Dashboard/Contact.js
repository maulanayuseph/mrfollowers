import React,{useState} from 'react';
import "../../App.scss";
import "../../AppMobile.scss";
import { connect } from "react-redux";
import { BallBeat } from 'react-pure-loaders';
import pengusaha from "../../assets/img/home/html/pengusahailustrasi.svg"
import pegawai from "../../assets/img/home/html/pekerjailustrasi.svg"
import berita1 from "../../assets/img/home/thought-catalog-UK78i6vK3sc-unsplash@2x.png"
import berita2 from "../../assets/img/home/glenn-carstens-peters-SL5d_8ywAAA-unsplash@2x.png"
import berita3 from "../../assets/img/home/dayne-topkin-y5_mFlLMwJk-unsplash@2x.png"
import wa from '../../assets/img/wa.png'
import ig from '../../assets/img/ig.jpg'
import gm from '../../assets/img/gm.png'
import fb from '../../assets/img/fb.png'
import Background1 from '../../assets/img/slide1.jpg'
import Background2 from '../../assets/img/slide2.jpg'
import Background3 from '../../assets/img/slide3.jpg'
import Background4 from '../../assets/img/slide4.jpg'
import Background5 from '../../assets/img/slide5.jpg'
import aboutus from '../../assets/img/aboutus.jpg'
import Navbar from "../../components/navbar_home";
import Footer from "../../components/Footer";
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Carousel from 'react-bootstrap/Carousel'
import slideimg from '../../assets/img/home/smartphone_mockup@2x.png';
import terlaris from '../../assets/img/home/html/onstore/Group 15276.svg';
import $ from 'jquery'

import { Divider, Image, Icon, Grid, Menu, Segment, Header, Card } from 'semantic-ui-react'

class Contact extends React.Component {  
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoading: true,
      data:[]
    };
  }
  
  componentDidMount() {
    this.setState({isLoading:true});
}
  
	render() {
    
		return(
      <div className=''>
          <Navbar/>
          <HeaderDiv/>
          <br/>
          <Footer/>
      </div>
		);
	}
  
}

function HeaderDiv() {
  return (
    <section id="intro">

        <div className="intro-content">
        <Card.Group itemsPerRow={4} className="cardKontak">
            <Card raised color='red'>
                    <div className="ui fluid image small">
                    <Image src={wa} wrapped ui={false} size="mini" />
                </div>
                <Card.Content>
                <Card.Header>WhatsApp</Card.Header>
                <Card.Meta style={{color:'#13952e'}}>
                    <a style={{color:'#13952e'}} href="https://api.whatsapp.com/send?phone=6285846104311&text=Halo saya mau pesan..">+6285846104311</a>
                </Card.Meta>
                </Card.Content>
            </Card>
            <Card raised color='red'>
                    <div className="ui fluid image small">
                    <Image src={gm} wrapped ui={false}  size="mini"/>
                </div>
                <Card.Content>
                <Card.Header>Email</Card.Header>
                <Card.Meta style={{color:'#13952e'}}>
                    <a style={{color:'#13952e'}} href="mailto:admin.adsmrfollowers@gmail.com">adsmrfollowers</a>
                </Card.Meta>
                </Card.Content>
            </Card>
            <Card raised color='red'>
                    <div className="ui fluid image small">
                    <Image src={ig} wrapped ui={false}  size="mini"/>
                </div>
                <Card.Content>
                <Card.Header>Instagram</Card.Header>
                <Card.Meta style={{color:'#13952e'}}>
                    <a style={{color:'#13952e'}} href="https://instagram.com/adsmrfollowers">adsmrfollowers</a>
                </Card.Meta>
                </Card.Content>
            </Card>
            <Card raised color='red'>
                    <div className="ui fluid image small">
                    <Image src={fb} wrapped ui={false}  size="mini"/>
                </div>
                <Card.Content>
                <Card.Header>Facebook</Card.Header>
                <Card.Meta style={{color:'#13952e'}}>
                    <a style={{color:'#13952e'}} href="https://www.facebook.com/adsmrfollowers">adsmrfollowers</a>
                </Card.Meta>
                </Card.Content>
            </Card>
        </Card.Group>


        {/* Mobile Only */}
        <Card.Group itemsPerRow={1} className="cardKontakMobile">
            <Card raised color='red'>
                    <div className="ui fluid image small">
                    <Image src={wa} wrapped ui={false} size="mini" />
                </div>
                <Card.Content>
                <Card.Header>WhatsApp</Card.Header>
                <Card.Meta style={{color:'#13952e'}}>
                    <a style={{color:'#13952e'}} href="https://api.whatsapp.com/send?phone=6285846104311&text=Halo saya mau pesan..">+6285846104311</a>
                </Card.Meta>
                </Card.Content>
            </Card>
        </Card.Group>
        </div>

        <div id="intro-carousel" className="owl-carousel" >
          <div className="item" style={{backgroundImage:"url(" + Background1 + ")"}}></div>
          <div className="item" style={{backgroundImage:"url(" + Background2 + ")"}}></div>
          <div className="item" style={{backgroundImage:"url(" + Background3 + ")"}}></div>
          <div className="item" style={{backgroundImage:"url(" + Background4 + ")"}}></div>
          <div className="item" style={{backgroundImage:"url(" + Background5 + ")"}}></div>
        </div>

      </section>
  );
}


function mapStateToProps(state) {
  return { loginUser: state.authReducer.loginUser }
}

function mapDispatchToProps(dispatch) {
  return { dispatch }
}

export default connect(mapStateToProps, mapDispatchToProps)(Contact);
