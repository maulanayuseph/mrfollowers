import React,{useState} from 'react';
import "../../App.scss";
import "../../AppMobile.scss";
import { connect } from "react-redux";
import { BallBeat } from 'react-pure-loaders';
import pengusaha from "../../assets/img/home/html/pengusahailustrasi.svg"
import pegawai from "../../assets/img/home/html/pekerjailustrasi.svg"
import berita1 from "../../assets/img/home/thought-catalog-UK78i6vK3sc-unsplash@2x.png"
import berita2 from "../../assets/img/home/glenn-carstens-peters-SL5d_8ywAAA-unsplash@2x.png"
import berita3 from "../../assets/img/home/dayne-topkin-y5_mFlLMwJk-unsplash@2x.png"
import wa from '../../assets/img/wa.png'
import ig from '../../assets/img/ig.jpg'
import gm from '../../assets/img/gm.png'
import fb from '../../assets/img/fb.png'
import Background1 from '../../assets/img/sampultokped.jpg'
import ig_paket from '../../assets/img/tokped.jpg'
import Navbar from "../../components/navbar_home";
import Footer from "../../components/Footer";
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Carousel from 'react-bootstrap/Carousel'
import slideimg from '../../assets/img/home/smartphone_mockup@2x.png';
import terlaris from '../../assets/img/home/html/onstore/Group 15276.svg';
import $ from 'jquery'

import { Divider, Image, Icon, Grid, Menu, Segment, Header, Card , Button} from 'semantic-ui-react'

class Instagram extends React.Component {  
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoading: true,
      data:[]
    };
  }
  
  componentDidMount() {
    this.setState({isLoading:true});
}
  
	render() {
    
		return(
      <div className=''>
          <Navbar/>
          <HeaderDiv/>
          <Paket/>
          <br/><br/>
          <Footer/>
      </div>
		);
	}
  
}

function HeaderDiv() {
  return (
    <section id="intro">

        <div className="intro-content">
          <h2>Followers Tokopedia</h2>
          <h1 className="headtextOrder">
            <i>
                Kembangkan marketplace tokopedia anda dengan beberapa layanan <br/> yang kami punya !
          </i>
          </h1>
        </div>

        <div id="intro-carousel" className="owl-carousel" >
          <div className="item" style={{backgroundImage:"url(" + Background1 + ")"}}></div>
        </div>

      </section>
  );
}

function Paket() {
    return (
        <Container style={{paddingTop:'20px'}}>
        <Row>
        <Col xs="12" md="12" lg="12">
        <Card.Group itemsPerRow={4} className="cardPaket">
            <Card raised color='green' style={{textAlign:'center'}}>
                <Image src={ig_paket} wrapped ui={false}/>
                <Card.Content>
                <Card.Header>Followers Tokopedia</Card.Header>
                <Card.Meta style={{color:'#13952e'}}>
                    500 Followers
                </Card.Meta>
                <Divider/>
                <Card.Meta>
                    <Header as="h2" style={{color:'#13952e',textAlign:'center'}}>500 Followers</Header>
                </Card.Meta>
                <Card.Meta>
                    <Header as="h2" style={{color:'#13952e',textAlign:'center'}}>Rp. 10.000</Header>
                </Card.Meta>
                <Card.Description style={{textAlign:'center'}}>
                    <Segment attached>
                    <b>Meningkatkan popularitas akun</b>
                    </Segment>
                    <Segment attached>
                    <b>Hanya Butuh link akun Tokopedia</b>
                    </Segment>
                    <Segment attached>
                    Proses 1 - 2 Hari
                    </Segment>
                </Card.Description>
                </Card.Content>
                <Card.Content extra>
                <Button color='green' href="/order-followers-tokopedia">
                    Ya, Saya mau yang ini
                </Button>
                </Card.Content>
            </Card>
            <Card raised color='green' style={{textAlign:'center'}}>
                <Image src={ig_paket} wrapped ui={false}/>
                <Card.Content>
                <Card.Header>Followers Tokopedia</Card.Header>
                <Card.Meta style={{color:'#13952e'}}>
                    1000 Followers
                </Card.Meta>
                <Divider/>
                <Card.Meta>
                    <Header as="h2" style={{color:'#13952e',textAlign:'center'}}>1000 Followers</Header>
                </Card.Meta>
                <Card.Meta>
                    <Header as="h2" style={{color:'#13952e',textAlign:'center'}}>Rp. 20.000</Header>
                </Card.Meta>
                <Card.Description style={{textAlign:'center'}}>
                    <Segment attached>
                    <b>Meningkatkan popularitas akun</b>
                    </Segment>
                    <Segment attached>
                    <b>Hanya Butuh link akun Tokopedia</b>
                    </Segment>
                    <Segment attached>
                    Proses 1 - 2 Hari
                    </Segment>
                </Card.Description>
                </Card.Content>
                <Card.Content extra>
                <Button color='green' href="/order-followers-tokopedia">
                    Ya, Saya mau yang ini
                </Button>
                </Card.Content>
            </Card>
            <Card raised color='green' style={{textAlign:'center'}}>
                <Image src={ig_paket} wrapped ui={false}/>
                <Card.Content>
                <Card.Header>Followers Tokopedia</Card.Header>
                <Card.Meta style={{color:'#13952e'}}>
                    2000 Followers
                </Card.Meta>
                <Divider/>
                <Card.Meta>
                    <Header as="h2" style={{color:'#13952e',textAlign:'center'}}>2000 Followers</Header>
                </Card.Meta>
                <Card.Meta>
                    <Header as="h2" style={{color:'#13952e',textAlign:'center'}}>Rp. 40.000</Header>
                </Card.Meta>
                <Card.Description style={{textAlign:'center'}}>
                    <Segment attached>
                    <b>Meningkatkan popularitas akun</b>
                    </Segment>
                    <Segment attached>
                    <b>Hanya Butuh link akun Tokopedia</b>
                    </Segment>
                    <Segment attached>
                    Proses 1 - 2 Hari
                    </Segment>
                </Card.Description>
                </Card.Content>
                <Card.Content extra>
                <Button color='green' href="/order-followers-tokopedia">
                    Ya, Saya mau yang ini
                </Button>
                </Card.Content>
            </Card>
            <Card raised color='green' style={{textAlign:'center'}}>
                <Image src={ig_paket} wrapped ui={false}/>
                <Card.Content>
                <Card.Header>Followers Tokopedia</Card.Header>
                <Card.Meta style={{color:'#13952e'}}>
                    5000 Followers
                </Card.Meta>
                <Divider/>
                <Card.Meta>
                    <Header as="h2" style={{color:'#13952e',textAlign:'center'}}>5000 Followers</Header>
                </Card.Meta>
                <Card.Meta>
                    <Header as="h2" style={{color:'#13952e',textAlign:'center'}}>Rp. 100.000</Header>
                </Card.Meta>
                <Card.Description style={{textAlign:'center'}}>
                    <Segment attached>
                    <b>Meningkatkan popularitas akun</b>
                    </Segment>
                    <Segment attached>
                    <b>Hanya Butuh link akun Tokopedia</b>
                    </Segment>
                    <Segment attached>
                    Proses 1 - 2 Hari
                    </Segment>
                </Card.Description>
                </Card.Content>
                <Card.Content extra>
                <Button color='green' href="/order-followers-tokopedia">
                    Ya, Saya mau yang ini
                </Button>
                </Card.Content>
            </Card>
            <Card raised color='green' style={{textAlign:'center'}}>
                <Image src={ig_paket} wrapped ui={false}/>
                <Card.Content>
                <Card.Header>Followers Tokopedia</Card.Header>
                <Card.Meta style={{color:'#13952e'}}>
                    10000 Followers
                </Card.Meta>
                <Divider/>
                <Card.Meta>
                    <Header as="h2" style={{color:'#13952e',textAlign:'center'}}>10000 Followers</Header>
                </Card.Meta>
                <Card.Meta>
                    <Header as="h2" style={{color:'#13952e',textAlign:'center'}}>Rp. 200.000</Header>
                </Card.Meta>
                <Card.Description style={{textAlign:'center'}}>
                    <Segment attached>
                    <b>Meningkatkan popularitas akun</b>
                    </Segment>
                    <Segment attached>
                    <b>Hanya Butuh link akun Tokopedia</b>
                    </Segment>
                    <Segment attached>
                    Proses 1 - 2 Hari
                    </Segment>
                </Card.Description>
                </Card.Content>
                <Card.Content extra>
                <Button color='green' href="/order-followers-tokopedia">
                    Ya, Saya mau yang ini
                </Button>
                </Card.Content>
            </Card>
        </Card.Group>

        {/* Mobile */}
        <Card.Group itemsPerRow={1} className="cardPaketMobile">
        <Card raised color='green' style={{textAlign:'center'}}>
                <Image src={ig_paket} wrapped ui={false}/>
                <Card.Content>
                <Card.Header>Followers Tokopedia</Card.Header>
                <Card.Meta style={{color:'#13952e'}}>
                    500 Followers
                </Card.Meta>
                <Divider/>
                <Card.Meta>
                    <Header as="h2" style={{color:'#13952e',textAlign:'center'}}>500 Followers</Header>
                </Card.Meta>
                <Card.Meta>
                    <Header as="h2" style={{color:'#13952e',textAlign:'center'}}>Rp. 10.000</Header>
                </Card.Meta>
                <Card.Description style={{textAlign:'center'}}>
                    <Segment attached>
                    <b>Meningkatkan popularitas akun</b>
                    </Segment>
                    <Segment attached>
                    <b>Hanya Butuh link akun Tokopedia</b>
                    </Segment>
                    <Segment attached>
                    Proses 1 - 2 Hari
                    </Segment>
                </Card.Description>
                </Card.Content>
                <Card.Content extra>
                <Button color='green' href="/order-followers-tokopedia">
                    Ya, Saya mau yang ini
                </Button>
                </Card.Content>
            </Card>
            <Card raised color='green' style={{textAlign:'center'}}>
                <Image src={ig_paket} wrapped ui={false}/>
                <Card.Content>
                <Card.Header>Followers Tokopedia</Card.Header>
                <Card.Meta style={{color:'#13952e'}}>
                    1000 Followers
                </Card.Meta>
                <Divider/>
                <Card.Meta>
                    <Header as="h2" style={{color:'#13952e',textAlign:'center'}}>1000 Followers</Header>
                </Card.Meta>
                <Card.Meta>
                    <Header as="h2" style={{color:'#13952e',textAlign:'center'}}>Rp. 20.000</Header>
                </Card.Meta>
                <Card.Description style={{textAlign:'center'}}>
                    <Segment attached>
                    <b>Meningkatkan popularitas akun</b>
                    </Segment>
                    <Segment attached>
                    <b>Hanya Butuh link akun Tokopedia</b>
                    </Segment>
                    <Segment attached>
                    Proses 1 - 2 Hari
                    </Segment>
                </Card.Description>
                </Card.Content>
                <Card.Content extra>
                <Button color='green' href="/order-followers-tokopedia">
                    Ya, Saya mau yang ini
                </Button>
                </Card.Content>
            </Card>
            <Card raised color='green' style={{textAlign:'center'}}>
                <Image src={ig_paket} wrapped ui={false}/>
                <Card.Content>
                <Card.Header>Followers Tokopedia</Card.Header>
                <Card.Meta style={{color:'#13952e'}}>
                    2000 Followers
                </Card.Meta>
                <Divider/>
                <Card.Meta>
                    <Header as="h2" style={{color:'#13952e',textAlign:'center'}}>2000 Followers</Header>
                </Card.Meta>
                <Card.Meta>
                    <Header as="h2" style={{color:'#13952e',textAlign:'center'}}>Rp. 40.000</Header>
                </Card.Meta>
                <Card.Description style={{textAlign:'center'}}>
                    <Segment attached>
                    <b>Meningkatkan popularitas akun</b>
                    </Segment>
                    <Segment attached>
                    <b>Hanya Butuh link akun Tokopedia</b>
                    </Segment>
                    <Segment attached>
                    Proses 1 - 2 Hari
                    </Segment>
                </Card.Description>
                </Card.Content>
                <Card.Content extra>
                <Button color='green' href="/order-followers-tokopedia">
                    Ya, Saya mau yang ini
                </Button>
                </Card.Content>
            </Card>
            <Card raised color='green' style={{textAlign:'center'}}>
                <Image src={ig_paket} wrapped ui={false}/>
                <Card.Content>
                <Card.Header>Followers Tokopedia</Card.Header>
                <Card.Meta style={{color:'#13952e'}}>
                    5000 Followers
                </Card.Meta>
                <Divider/>
                <Card.Meta>
                    <Header as="h2" style={{color:'#13952e',textAlign:'center'}}>5000 Followers</Header>
                </Card.Meta>
                <Card.Meta>
                    <Header as="h2" style={{color:'#13952e',textAlign:'center'}}>Rp. 100.000</Header>
                </Card.Meta>
                <Card.Description style={{textAlign:'center'}}>
                    <Segment attached>
                    <b>Meningkatkan popularitas akun</b>
                    </Segment>
                    <Segment attached>
                    <b>Hanya Butuh link akun Tokopedia</b>
                    </Segment>
                    <Segment attached>
                    Proses 1 - 2 Hari
                    </Segment>
                </Card.Description>
                </Card.Content>
                <Card.Content extra>
                <Button color='green' href="/order-followers-tokopedia">
                    Ya, Saya mau yang ini
                </Button>
                </Card.Content>
            </Card>
            <Card raised color='green' style={{textAlign:'center'}}>
                <Image src={ig_paket} wrapped ui={false}/>
                <Card.Content>
                <Card.Header>Followers Tokopedia</Card.Header>
                <Card.Meta style={{color:'#13952e'}}>
                    10000 Followers
                </Card.Meta>
                <Divider/>
                <Card.Meta>
                    <Header as="h2" style={{color:'#13952e',textAlign:'center'}}>10000 Followers</Header>
                </Card.Meta>
                <Card.Meta>
                    <Header as="h2" style={{color:'#13952e',textAlign:'center'}}>Rp. 200.000</Header>
                </Card.Meta>
                <Card.Description style={{textAlign:'center'}}>
                    <Segment attached>
                    <b>Meningkatkan popularitas akun</b>
                    </Segment>
                    <Segment attached>
                    <b>Hanya Butuh link akun Tokopedia</b>
                    </Segment>
                    <Segment attached>
                    Proses 1 - 2 Hari
                    </Segment>
                </Card.Description>
                </Card.Content>
                <Card.Content extra>
                <Button color='green' href="/order-followers-tokopedia">
                    Ya, Saya mau yang ini
                </Button>
                </Card.Content>
            </Card>
        </Card.Group>
        </Col>
        </Row>
        </Container>
    )
}


function mapStateToProps(state) {
  return { loginUser: state.authReducer.loginUser }
}

function mapDispatchToProps(dispatch) {
  return { dispatch }
}

export default connect(mapStateToProps, mapDispatchToProps)(Instagram);
