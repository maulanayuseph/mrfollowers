import React,{useState} from 'react';
import "../../App.scss";
import "../../AppMobile.scss";
import { connect } from "react-redux";
import { BallBeat } from 'react-pure-loaders';
import pengusaha from "../../assets/img/home/html/pengusahailustrasi.svg"
import pegawai from "../../assets/img/home/html/pekerjailustrasi.svg"
import berita1 from "../../assets/img/home/thought-catalog-UK78i6vK3sc-unsplash@2x.png"
import berita2 from "../../assets/img/home/glenn-carstens-peters-SL5d_8ywAAA-unsplash@2x.png"
import berita3 from "../../assets/img/home/dayne-topkin-y5_mFlLMwJk-unsplash@2x.png"
import wa from '../../assets/img/wa.png'
import ig from '../../assets/img/ig.jpg'
import gm from '../../assets/img/gm.png'
import fb from '../../assets/img/fb.png'
import Background1 from '../../assets/img/slide1.jpg'
import { BASE_URL } from "../../service/api";
import ig_paket from '../../assets/img/instagram-1882330_1280.png'
import Navbar from "../../components/navbar_home";
import Footer from "../../components/Footer";
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Carousel from 'react-bootstrap/Carousel'
import slideimg from '../../assets/img/home/smartphone_mockup@2x.png';
import terlaris from '../../assets/img/home/html/onstore/Group 15276.svg';
import $ from 'jquery'
import axios from 'axios'

import { Divider, Image, Icon, Grid, Menu, Segment, Header, Card , Button, Form, Label, Table} from 'semantic-ui-react'

class CekOrder extends React.Component {  
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoading: true,
      data:[],
      dataResponses : [],
      noinv : '',
      msgError : '',
    };
  }
  
  componentDidMount() {
    this.setState({isLoading:true});
}
  
	render() {
        
        const form = {
            noinv : this.state.noinv,
        }
        
        const handleTextChange = event => this.setState({
            [event.target.name]: event.target.value
        });

        async function handleInputSubmit () {

            if(form.noinv === "") {
                $("#errnoinv").removeAttr('style');
                window.setTimeout(function(){
                    $("#errnoinv").attr('style','display:none')
                },2000)
                return false;
            }

            submitOrder(form)
        }

        const submitOrder = async (values) => {
            $("#btnOrder").addClass('loading');
            $("#btnOrder").addClass('disabled');

            let config = {
                headers: {
                  'content-type': 'application/json',
                  'Access-Control-Allow-Origin': '*',
                  "Access-Control-Allow-Headers" : "Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With",
                  "Access-Control-Allow-Methods" : "POST"
                },
                auth : {
                    username : 'web1',
                    password : 'web1'
                }
            }

            axios.post(BASE_URL+"/CekOrder", values, config)
            .then((resp) => {
              
                if (resp.data.status != 'success') {
                    $("#btnOrder").removeClass('loading');
                    $("#btnOrder").removeClass('disabled');
                    $("#errnotfound").removeAttr('style');
                    $(".tblOrder").attr('style','display:none');

                    setTimeout(() => {
                        $("#errnotfound").attr('style','display:none');
                    }, 2000)
                } else {
                    $("#btnOrder").removeClass('loading');
                    $("#btnOrder").removeClass('disabled');
                    $("#classSuccess").removeAttr('style');
                    $(".tblOrder").removeAttr('style');
                    this.setState({ 
                        dataResponses : resp.data.data
                    })
                }
            })
            .catch((resp) =>{
                $("#errnotfound").removeAttr('style');
                $("#btnOrder").removeClass('loading');
                $("#btnOrder").removeClass('disabled');
                $(".tblOrder").attr('style','display:none');
                
                setTimeout(() => {
                    $("#errnotfound").attr('style','display:none');
                }, 2000)

            });
            
        }

		return(
      <div className=''>
          <Navbar/>
          <HeaderDiv/>
          <Paket form={form} handleTextChange={handleTextChange} handleInputSubmit={handleInputSubmit} dataResponses={this.state.dataResponses}/>
          <br/><br/>
          <Footer/>
      </div>
		);
	}
  
}

function HeaderDiv() {
  return (
    <section id="intro">

        <div className="intro-content">
          <h2>Cek Pesanan</h2>
          <h1 className="headtextOrder">
            <i>
                Cek Pesanan anda dengan mengisi Nomor Invoice Pemesanan dibawah ini 
          </i>
          </h1>
        </div>

        <div id="intro-carousel" className="owl-carousel" >
          <div className="item" style={{backgroundImage:"url(" + Background1 + ")"}}></div>
        </div>

      </section>
  );
}

function Paket({form,handleTextChange,handleInputSubmit,dataResponses}) {
    return (
        <Container style={{paddingTop:'20px'}}>
        <Row>
        <Col xs="12" md="12" lg="12">
            <Header as="h1">
                Form Cek Pesanan
            </Header>
            <Form>
                <Form.Field>
                <label>No Invoice *</label>
                <input placeholder='Masukkan Nomor Invoice Anda' name="noinv" value={form.noinv} onChange={handleTextChange} />
                <Label basic color='red' pointing id="errnoinv" style={{display:'none'}}>
                    Nomor Invoice Wajib Diisi
                </Label>
                </Form.Field>
                <Button onClick={handleInputSubmit} className="col-md-1" color="green" circular id="btnOrder">Check</Button>
            </Form>

            <Label basic color='red' pointing id="errnotfound" style={{display:'none'}}>
                Data Tidak Ditemukan
            </Label>

            <Table className="tblOrder" celled style={{display:'none'}}>
                <Table.Header>
                <Table.Row>
                    <Table.HeaderCell>Invoice</Table.HeaderCell>
                    <Table.HeaderCell>Email</Table.HeaderCell>
                    <Table.HeaderCell>No Handphone</Table.HeaderCell>
                    <Table.HeaderCell>Status</Table.HeaderCell>
                </Table.Row>
                </Table.Header>

                <Table.Body>
                <Table.Row positive>
                    <Table.Cell>
                    <Label ribbon green>{dataResponses.invoice}</Label>
                    </Table.Cell>
                    <Table.Cell>{dataResponses.email}</Table.Cell>
                    <Table.Cell>{dataResponses.no_tlp}</Table.Cell>
                    <Table.Cell>{dataResponses.status}</Table.Cell>
                </Table.Row>
                </Table.Body>
            </Table>
        </Col>
        </Row>
        </Container>
    )
}


function mapStateToProps(state) {
  return { loginUser: state.authReducer.loginUser }
}

function mapDispatchToProps(dispatch) {
  return { dispatch }
}

export default connect(mapStateToProps, mapDispatchToProps)(CekOrder);
