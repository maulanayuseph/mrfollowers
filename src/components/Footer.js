
import React, { Component } from 'react';
import "../App.scss";
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import $ from 'jquery'
import bni from '../assets/img/bni.png'
import mandiri from '../assets/img/mandiri.jpg'
import ovo from '../assets/img/ovo.png'
import gopay from '../assets/img/gopay.jpg'
import dana from '../assets/img/dana.png'
import wa from '../assets/img/wa.png'
import ig from '../assets/img/ig.jpg'
import gm from '../assets/img/gm.png'
import fb from '../assets/img/fb.png'

import { Divider, Image, Icon,Grid, Header, Card, Button } from 'semantic-ui-react'

function Footer() {
  $(window).scroll(function(){if($(this).scrollTop()>=50){$('#return-to-top').fadeIn(200);}else{$('#return-to-top').fadeOut(200);}});$('#return-to-top').click(function(){$('body,html').animate({scrollTop:0},500);});
  return (
  <>
  <Container style={{paddingTop:'20px'}}>
  <Row>
    <Col xs="12" md="12" lg="12">
    <section id="call-to-action" className="wow fadeInUp" style={{background:'#32d446',borderRadius:'10px'}}>
      <div className="container">
          <div className="row">
          <div className="col-lg-9 text-center text-lg-left">
              <h3 className="cta-title">Menjadi Reseller</h3>
              <p className="cta-text"> 
                  Klik tombol selengkapnya, Lalu gunakan kode voucher <b>VOUCHERADSMR</b> pada saat pendaftaran.
              </p>
          </div>
          <div className="col-lg-3 cta-btn-container text-center">
              <a className="cta-btn align-middle" href="https://panel.resellerindo.com/reseller.php/login/register2">Selengkapnya</a>
          </div>
          </div>

      </div>
    </section>
    <br/><br/>
    </Col>
  </Row>
  </Container>
  <Container style={{paddingTop:'20px',paddingBottom:'50px'}}>
  <Row>
  <Col xs="12" md="12" lg="12" style={{textAlign:'center'}}>
    <a href="https://bit.ly/2ZZ3n0l"><img src="https://idcloudhost.com/wp-content/uploads/2017/01/970x250.png" height="250" width="970" border="0" alt="IDCloudHost | SSD Cloud Hosting Indonesia" /></a>
  </Col>
  </Row>
  </Container>
  <footer id="footer" style={{backgroundColor:'white'}}>
    <div className="container">
      <Grid columns={3}>
        <Grid.Row className="footerDesk footerText">
          <Grid.Column floated='left' width={5}>
            <Header as='h2'>Tentang Kami</Header>
            <Header as='h6'>Kami siap membantu kebutuhan anda dalam bersosial media dan bermarketplace.</Header>
            <Header as='h2'>Jam Kerja</Header>
            <Header as='h6'>Senin - Sabtu (08.00 - 20.00)
            <br/> Minggu (14.00 - 20.00)
            </Header>
            <Header as='h2'>Pembayaran</Header>
            <Image.Group size='tiny'>
              <Image src={bni} />
              <Image src={mandiri} />
              <Image src={ovo} />
              <Image src={gopay} />
              <Image src={dana} />
            </Image.Group>
          </Grid.Column>
          <Grid.Column floated='right' width={5}>
              <Header as='h2'>Kontak</Header>
              <div>
              <Icon.Group size='large'>
                <Icon name='whatsapp'  color='green'/>
              </Icon.Group>
              <a href="https://api.whatsapp.com/send?phone=6285846104311&text=Halo saya mau pesan.."> +62 858 4610 4311</a> <br/><br/>
              </div>
              <div>
              <Icon.Group size='large'>
                <Icon name='facebook'  color='blue'/>
              </Icon.Group>
              <a href="https://www.facebook.com/adsmrfollowers"> adsmrfollowers</a> <br/><br/>
              </div>
              <div>
              <Icon.Group size='large'>
                <Icon name='instagram'  color='pink'/>
              </Icon.Group>
              <a href="https://instagram.com/adsmrfollowers"> adsmrfollowers</a><br/><br/>
              </div>
              <div>
              <Icon.Group size='large'>
                <Icon name='mail'  color='red'/>
              </Icon.Group>
              <a href="mailto:admin.adsmrfollowers@gmail.com"> admin.adsmrfollowers@gmail.com</a><br/><br/>
              </div>
          </Grid.Column>
        </Grid.Row>
      </Grid>

      <Grid columns={3}>
        <Grid.Row className="footerMobile footerText">
          <Grid.Column className="col-12"  floated='left' width={5}>
            <Header as='h2'>Tentang Kami</Header>
            <Header as='h6'>Kami siap membantu kebutuhan anda dalam bersosial media dan bermarketplace.</Header>
            <Header as='h2'>Jam Kerja</Header>
            <Header as='h6'>Senin - Sabtu (08.00 - 20.00)
            <br/> Minggu (14.00 - 20.00)
            </Header>
            <Header as='h2'>Pembayaran</Header>
            <Image.Group size='tiny'>
              <Image src={bni} />
              <Image src={mandiri} />
              <Image src={ovo} />
              <Image src={gopay} />
              <Image src={dana} />
            </Image.Group>
          </Grid.Column>
          <Grid.Column className="col-12" floated='right' width={5}>
              <Header as='h2'>Kontak</Header>
              <div>
              <Icon.Group size='large'>
                <Icon name='whatsapp'  color='green'/>
              </Icon.Group>
              <a href="https://api.whatsapp.com/send?phone=6285846104311&text=Halo saya mau pesan.."> +62 858 4610 4311</a> <br/><br/>
              </div>
              <div>
              <Icon.Group size='large'>
                <Icon name='facebook'  color='blue'/>
              </Icon.Group>
              <a href="https://www.facebook.com/adsmrfollowers"> adsmrfollowers</a> <br/><br/>
              </div>
              <div>
              <Icon.Group size='large'>
                <Icon name='instagram'  color='pink'/>
              </Icon.Group>
              <a href="https://instagram.com/adsmrfollowers"> adsmrfollowers</a><br/><br/>
              </div>
              <div>
              <Icon.Group size='large'>
                <Icon name='mail'  color='red'/>
              </Icon.Group>
              <a href="mailto:admin.adsmrfollowers@gmail.com"> admin.adsmrfollowers@gmail.com</a><br/><br/>
              </div>
          </Grid.Column>
        </Grid.Row>
      </Grid>
      <div className="copyright">
        <strong>Ads Mr Followers</strong>. I made with fully love.
      </div>
    </div>
    <a href="#" className="back-to-top"><i className="fa fa-chevron-up"></i></a>
  </footer>
  </>
  );
}



export default Footer;
