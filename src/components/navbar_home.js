import React,{useState}  from "react";
import "../App.scss";
import { Navbar, Nav, NavDropdown, Button,Modal,Collapse } from "react-bootstrap";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  Redirect,
  useHistory,
  useLocation
} from "react-router-dom";
import $ from 'jquery'
import { Dropdown, Menu } from 'semantic-ui-react'

function Header() {

  return (
    <Router>
    
    <section id="topbar" className="d-none d-lg-block" style={{backgroundColor:'white'}}>
      <div className="container clearfix">
        <div className="contact-info float-left">
          <i className="fa fa-envelope-o"></i> <a href="mailto:admin@adsmrfollowers.my.id">admin@adsmrfollowers.my.id</a>
          <i className="fa fa-phone"></i> <a href="https://api.whatsapp.com/send?phone=6285846104311&text=Halo saya mau pesan..">+62 858 4610 4311</a>
        </div>
        <div className="social-links float-right">
          <a href="https://www.facebook.com/adsmrfollowers" className="facebook"><i className="fa fa-facebook"></i></a>
          <a href="https://instagram.com/adsmrfollowers" className="instagram"><i className="fa fa-instagram"></i></a>
        </div>
      </div>
    </section>

    <header id="header" style={{backgroundColor:'white'}}>
      <div className="container">

        <div id="logo" className="pull-left">
          <h1><a href="/" className="scrollto">Ads Mr <span>Followers</span></a></h1>
        </div>

        <nav id="nav-menu-container">
          <ul className="nav-menu">
            <li className="menu-active"><a href="/">Home</a></li>
            <li className="menu-has-children"><a href="#">Order</a>
              <ul>
                <li className="menu-has-children">
                  <a href="#">Instagram</a>
                  <ul>
                    <li><a href="/tambah-followers-instagram">Tambah Followers Instagram</a></li>
                    <li><a href="/likes-instagram">Likes Instagram</a></li>
                    <li><a href="/auto-likes-instagram">Auto Likes Instagram</a></li>
                  </ul>
                </li>
                <li className="menu-has-children">
                  <a href="#">Facebook</a>
                  <ul>
                    <li><a href="/post-likes-facebook">Tambah Posts Likes Facebook</a></li>
                    <li><a href="/page-likes-facebook">Tambah Pages Likes Facebook</a></li>
                  </ul>
                </li>
                <li className="menu-has-children">
                  <a href="#">Youtube</a>
                  <ul>
                    <li><a href="/subscriber-youtube">Tambah Subscriber Youtube</a></li>
                  </ul>
                </li>
                <li className="menu-has-children">
                  <a href="#">Tokopedia</a>
                  <ul>
                    <li><a href="/followers-tokopedia">Followers Tokopedia</a></li>
                    <li><a href="/wishlist-tokopedia">Wishlist / Favorite Tokopedia</a></li>
                  </ul>
                </li>
                <li className="menu-has-children">
                  <a href="#">Shopee</a>
                  <ul>
                    <li><a href="/followers-shopee">Followers Shopee</a></li>
                    <li><a href="/likes-shopee">Likes Produk Shopee</a></li>
                  </ul>
                </li>
                {/* <li><a href="#">Youtube</a></li>
                <li><a href="#">Facebook</a></li>
                <li><a href="#">Twitter</a></li>
                <li><a href="#">Tiktok</a></li>
                <li><a href="#">Soundcloud</a></li>
                <li><a href="#">Tokopedia</a></li>
                <li><a href="#">Shopee</a></li>
                <li><a href="#">Bukalapak</a></li> */}
              </ul>
            </li>
            <li><a href="#about">Tentang Kami</a></li>
            <li><a href="#services">Jasa</a></li>
            {/* <li><a href="#testimonials">Testimonial</a></li> */}
            <li><a href="/cek-order"><i><strong>Cek Pesanan</strong></i></a></li>
            <li><a href="/kontak">Kontak</a></li>
          </ul>
        </nav>
      </div>
    </header>

    </Router>
  );
}

export default Header;
