import React,{useState}  from "react";
import "../App.scss";
import { Navbar, Nav, NavDropdown, Button,Modal,Collapse } from "react-bootstrap";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  Redirect,
  useHistory,
  useLocation
} from "react-router-dom";
import $ from 'jquery'
import { Dropdown, Menu } from 'semantic-ui-react'

function Header() {

  return (
    <Router>
    
    <section id="topbar" className="d-none d-lg-block" style={{backgroundColor:'white'}}>
      <div className="container clearfix">
        <div className="contact-info float-left">
          <i className="fa fa-envelope-o"></i> <a href="mailto:adm.mrfollowers@gmail.com">adm.mrfollowers@gmail.com</a>
          <i className="fa fa-phone"></i> +1 5589 55488 55
        </div>
        <div className="social-links float-right">
          <a href="#" className="facebook"><i className="fa fa-facebook"></i></a>
          <a href="#" className="instagram"><i className="fa fa-instagram"></i></a>
        </div>
      </div>
    </section>

    <header id="header" style={{backgroundColor:'white'}}>
      <div className="container">

        <div id="logo" className="pull-left">
          <h1><a href="/" className="scrollto">Mr<span>Followers</span></a></h1>
        </div>

        <nav id="nav-menu-container">
          <ul className="nav-menu">
            <li className="menu-active"><a href="/">Home</a></li>
            <li className="menu-has-children"><a href="#">Order</a>
              <ul>
                <li className="menu-has-children">
                  <a href="#">Instagram</a>
                  <ul>
                    <li><a href="/tambah-followers-instagram">Tambah Followers Instagram</a></li>
                  </ul>
                </li>
                {/* <li><a href="#">Youtube</a></li>
                <li><a href="#">Facebook</a></li>
                <li><a href="#">Twitter</a></li>
                <li><a href="#">Tiktok</a></li>
                <li><a href="#">Soundcloud</a></li>
                <li><a href="#">Tokopedia</a></li>
                <li><a href="#">Shopee</a></li>
                <li><a href="#">Bukalapak</a></li> */}
              </ul>
            </li>
            <li><a href="/cek-order">Cek Order</a></li>
            <li><a href="/kontak">Kontak</a></li>
          </ul>
        </nav>
      </div>
    </header>

    </Router>
  );
}

export default Header;
