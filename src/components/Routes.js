import React from 'react';
import { connect } from "react-redux";
import { compose } from "redux";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";

import Home from '../pages/Dashboard/Home';
import RedirectLink from '../pages/Dashboard/RedirectLink'
import Contact from '../pages/Dashboard/Contact';
import CekOrder from '../pages/Dashboard/CekOrder';
import Instagram from '../pages/Dashboard/Instagram';
import Orderfollowig from "../pages/Dashboard/Orderfollowersig";
import LikesInstagram from "../pages/Dashboard/LikesInstagram";
import Orderlikesig from "../pages/Dashboard/Orderlikesig";
import Autolikesig from "../pages/Dashboard/AutoLikesInstagram";
import Orderautoig from "../pages/Dashboard/Orderautoig";
import Postlikefb from "../pages/Dashboard/Postlikefb";
import Orderlikefb from "../pages/Dashboard/Orderlikesfb";
import Pagelikefb from "../pages/Dashboard/Pagelikefb";
import Orderlikepagefb from "../pages/Dashboard/Orderlikepagefb";
import Subscriberyt from "../pages/Dashboard/Subscriberyt";
import Ordersubsyt from "../pages/Dashboard/Ordersubsyt";
import Followerstokped from "../pages/Dashboard/Followerstokped";
import Orderfollowerstokped from "../pages/Dashboard/Orderfollowerstokped";
import Wishlisttokped from "../pages/Dashboard/Wishlisttokped";
import Orderwishlisttokped from "../pages/Dashboard/Orderwishlisttokped";
import Followersshopee from "../pages/Dashboard/Followersshopee";
import Orderfollowersshopee from "../pages/Dashboard/Orderfollowersshopee";
import Likesshopee from "../pages/Dashboard/Likesshopee";
import Orderlikesshopee from "../pages/Dashboard/Orderlikesshopee";

function PrivateRoute({ component: Component, authed, ...rest }) {
  return (
    <Route
      {...rest}
      render={(props) => authed === true
        ? <Component {...props} />
        : <Redirect to={{ pathname: '/Signin', state: { from: props.location } }} />}
    />
  )
}

class Routes extends React.Component {
  constructor(props) {
    super(props);
    this.props=props;
  }

  render(){
    return (
      <Router >
        <Route exact path='/' component={Home}/>
        <Route path='/kontak' component={Contact}/>
        <Route path='/cek-order' component={CekOrder}/>
        <Route path='/tambah-followers-instagram' component={Instagram}/>
        <Route path='/order-followers-instagram' component={Orderfollowig}/>
        <Route path='/likes-instagram' component={LikesInstagram}/>
        <Route path='/order-likes-instagram' component={Orderlikesig}/>
        <Route path='/auto-likes-instagram' component={Autolikesig}/>
        <Route path='/order-auto-instagram' component={Orderautoig}/>
        <Route path='/post-likes-facebook' component={Postlikefb}/>
        <Route path='/order-likes-facebook' component={Orderlikefb}/>
        <Route path='/page-likes-facebook' component={Pagelikefb}/>
        <Route path='/order-likes-page-facebook' component={Orderlikepagefb}/>
        <Route path='/subscriber-youtube' component={Subscriberyt}/>
        <Route path='/order-subscriber-youtube' component={Ordersubsyt}/>
        <Route path='/followers-tokopedia' component={Followerstokped}/>
        <Route path='/order-followers-tokopedia' component={Orderfollowerstokped}/>
        <Route path='/wishlist-tokopedia' component={Wishlisttokped}/>
        <Route path='/order-wishlist-tokopedia' component={Orderwishlisttokped}/>
        <Route path='/followers-shopee' component={Followersshopee}/>
        <Route path='/order-followers-shopee' component={Orderfollowersshopee}/>
        <Route path='/likes-shopee' component={Likesshopee}/>
        <Route path='/order-likes-shopee' component={Orderlikesshopee}/>
        <Route path="/RedirectLink" component={RedirectLink} />
        { /* <Route path='/detail' component={Detail}/>
        <Route path='/home/selesai' component={Selesai}/>

        <PrivateRoute authed={this.props.auth.isLoggedInPengelola} path='/Userpengelola' component={UserPengelola} />
        <PrivateRoute authed={this.props.auth.isLoggedInPemodal} path='/Userpemodal' component={UserPemodal} /> */}
      </Router >
    );
  }

}

function mapStateToProps(state) {
  return { auth: state.authReducer.authData }
}

function mapDispatchToProps(dispatch) {
  return { dispatch }
}


export default connect(mapStateToProps, mapDispatchToProps)(Routes);