import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form'

import authReducer from "./auth.reducer";
import userReducer from "./user.reducer";
import videoReducer from "./video.reducer";

const reducers = {
    authReducer,
    userReducer,
    videoReducer,
    form: formReducer
};

const appReducer = combineReducers(reducers);

const rootReducer = (state, action) => {

    if (action.type === "USER_LOGGED_OUT_SUCCESS") {
        state = undefined
    }

    return appReducer(state, action);
}

export default rootReducer;
